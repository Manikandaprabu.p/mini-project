import { Component } from '@angular/core';
import { Router } from '@angular/router';
// import { MatDialogRef } from '@angular/material/dialog';rgba(0, 0, 255, 0.671)
import { Register } from 'src/app/model/register';
import { FormValidationService } from 'src/app/service/form-validation.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  userRegisterationMode:Register = new Register();

  constructor(public urlService:FormValidationService,
    public router:Router){}

    
  SubmitData(){

    // console.log("Success")
    this.urlService.createlogin(this.userRegisterationMode).subscribe(data=>{
    
     this.start()
     

    });
    this.urlService.getEmail(this.userRegisterationMode).subscribe(data1=>{

    });
    }
    start(){
      this.router.navigateByUrl("/");
    }
    
  
    
}
