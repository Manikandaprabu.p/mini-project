import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Port } from 'src/app/model/port';
import { FormValidationService } from 'src/app/service/form-validation.service';
import { ValidationServiceService } from 'src/app/service/validation-service.service';

@Component({
  selector: 'app-navication',
  templateUrl: './navication.component.html',
  styleUrls: ['./navication.component.css']
})
export class NavicationComponent {
  userRegisterationMode:Port = new Port();

  constructor( public urlService:FormValidationService,
    public validationService:ValidationServiceService,
    public dialogReference:MatDialogRef<NavicationComponent>){}
   
 
  SubmitData(){

    // console.log("Success")
    this.urlService.createRegisteration(this.userRegisterationMode).subscribe(data=>{
    
    });
    this.onClose()
  }
    onClose(){
       this.dialogReference.close();
    }


  }

