import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Register } from 'src/app/model/register';
// import { FormValidationService } from 'src/app/service/form-validation.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  private URLRegistration="http://localhost:8080";
  
  newuser:Register = new Register();
 
constructor(private router: Router, private httpClient: HttpClient){}

userForms:FormGroup=new FormGroup({
  username:new FormControl('',Validators.required),
  password: new FormControl('',Validators.required),
});

 validatedata(){
  console.log(this.newuser.username);
  // const name = this.newuser.username
  // console.log("name",name);
  this.httpClient.post(this.URLRegistration+"/hello/login",this.newuser)
       .subscribe(
        (response) => {
          console.log('successfully logged');
          this.userlist();
        },
        (error) => {
          alert('Invalid Username and Password');
        }
      );
  }

  userlist(){
    this.router.navigateByUrl('/login');

  }

}      



