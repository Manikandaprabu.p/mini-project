import { Component } from '@angular/core';
import { Chart } from 'chart.js';
import { Db2dao } from 'src/app/model/db2dao';
import { FormValidationService } from 'src/app/service/form-validation.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent {
  mongoModel:Db2dao = new Db2dao()

   constructor(public formValidationService: FormValidationService){}
   Status:string[] = [''];
   Result:number[]=[];



  public chart:any;
  ngOnInit(){
    
    this.formValidationService.getMongoDetails().subscribe(data=>{
    
    console.log("DD",data);
    // this.model = data;
    data.forEach((value: Db2dao, index: number)=>{
     this.Status[index] = value.status as string;
     this.Result[index] = value.result as number;
    })



    });

    this.chart = new Chart('mychart', {
      
        type:'line',
        data: {
          labels: this.Status,
          datasets: [{
            label: 'port',
            data: this.Result,
            // backgroundColor: [
              // 'rgba(255, 99, 132, 0.2)',
              // 'rgba(255, 159, 64, 0.2)',
              // 'rgba(255, 205, 86, 0.2)',
              // 'rgba(75, 192, 192, 0.2)',
              // 'rgba(54, 162, 235, 0.2)',
              // 'rgba(153, 102, 255, 0.2)',
              // 'rgba(201, 203, 207, 0.2)'
             
              
            // ],
            fill:true,
            borderColor:[
              'rgb(75, 192, 192)',
            ]
           

    }],     
   
  },
  options:{
    aspectRatio:5.5
    }
  

});
}
}




