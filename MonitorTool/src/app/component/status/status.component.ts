import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NavicationComponent } from '../navication/navication.component';
import { Port } from 'src/app/model/port';
import { FormValidationService } from 'src/app/service/form-validation.service';


@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent {
  constructor(private diolag:MatDialog,
    public formValidationService:FormValidationService ){}

  // displayedColumns: string[] = ['ip', 'port', 'address', 'projectname'];
  // dataSource = ELEMENT_DATA;

  user(){
      this.diolag.open(NavicationComponent, {
        width:"80%",
        height:"50%",
      });
    }


  l:Port[] = [];
  datasource:any[] = []
ngOnInit(){
 
  this.formValidationService.getDetails().subscribe(data=> {
    data.forEach((w)=>{
      let element = new Port()
      element.ip = w.ip ;
      element.port = w.port ;
      element.address = w.address ;
      element.projectname = w.projectname;
     
      this.l.push(element);
      
      console.log(this.l);
      
        });
        this.datasource = this.l;  
  })
  


}

displayedColumns: string[] = ['ip', 'port', 'address', 'projectname'];

}
