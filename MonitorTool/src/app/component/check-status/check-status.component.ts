import { Component } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Db2dao } from 'src/app/model/db2dao';
import { Port } from 'src/app/model/port';
import { FormValidationService } from 'src/app/service/form-validation.service';

@Component({
  selector: 'app-check-status',
  templateUrl: './check-status.component.html',
  styleUrls: ['./check-status.component.css']
})
export class CheckStatusComponent {
  mongoModel:Db2dao = new Db2dao()
  data: any;
  data1: any;
  portStatus: any;
  datasource1: any = [];
  lastPort: number | undefined = undefined; 
  status: string | undefined;

  lastIp: string | undefined;
  condition: string | undefined;
  result:number | undefined;

   
 
  constructor(public formValidationService: FormValidationService,
    public urlService:FormValidationService) { }
  l: Port[] = [];
  datasource: Port[] = [];

  ngOnInit() {
    let port = Port;
    // console.log("port", port);
    let element = new Port();

    this.formValidationService.getDetails().subscribe(data => {
      this.datasource1 = new MatTableDataSource(data);
      const element = data[data.length - 1];
      const element1 = data[data.length - 1];
      const lastPort = element.port;
      const lastIp = element1.ip;
      this.lastPort = lastPort;
      this.lastIp = lastIp;

      // console.log("Last Port:", this.lastPort);
      // console.log("Last Ip:" , this.lastIp);

      if (this.lastPort !== undefined && this.lastIp !== undefined)  {
        this.formValidationService.get(element).subscribe(data1 => {
          this.portStatus = data1;
          this.datasource = this.l;
          this.datasource[0] = this.portStatus;
          element.s = this.datasource[0].status;
          this.l.push(element);
          // console.log( "std",element.s);
          this.status = element.s;
          // console.log(this.status)
          this.mongoModel.status=element.s;
          console.log("st",this.mongoModel.status);
          this.condition = this.mongoModel.status;
          console.log("condition",this.condition)


           
            const condition =this.condition == "ACTIVE NOW";
            this.result = condition ? 50 : -50;
            this.mongoModel.result =this.result;
            console.log("con",this.mongoModel.result);
          
         
                     
    this.urlService.createmongo(this.mongoModel).subscribe(data=>{
    });
   
        });
      }
     });
   
}
mail(){
  this.urlService.createKafka(this.mongoModel).subscribe(Response=>{
  });
}
  

  displayedColumns: string[] = ['status'];
  displayedColumns1: string[] = ['ip', 'port'];
}



function mail() {
  throw new Error('Function not implemented.');
}



