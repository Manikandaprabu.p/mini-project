import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTableModule} from '@angular/material/table';

const materialComponents=[
  MatButtonModule,
  MatGridListModule,
  MatInputModule,
  MatCardModule,
  MatToolbarModule,
  MatDialogModule,
  HttpClientModule,
  MatFormFieldModule,
  MatTableModule

     
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    materialComponents
  ],

  exports : [
    materialComponents
  ]
})
export class MaterialDesignModule { }
