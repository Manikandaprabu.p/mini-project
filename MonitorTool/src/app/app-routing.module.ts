import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { WelcomeComponent } from './component/welcome/welcome.component';
import { StatusComponent } from './component/status/status.component';
import { ChartComponent } from './component/chart/chart.component';
import { CheckStatusComponent } from './component/check-status/check-status.component';

const routes: Routes = [
{
  path:"",component:LoginComponent
},
{
  path:"register",component:RegisterComponent
},
{
  path:"",component:LoginComponent
},
{
  path:"login",component:WelcomeComponent
},
{
  path:"add",component:StatusComponent
},
{
  path:"chart",component:ChartComponent
},
{
  path:"status",component:CheckStatusComponent
}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
