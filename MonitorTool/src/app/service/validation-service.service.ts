import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationServiceService {

  constructor() { }

  userForms:FormGroup = new FormGroup({
   projectname:new FormControl('',[Validators.required,Validators.maxLength(6)]),
   port:new FormControl('',[Validators.required]),
   ip:new FormControl('',[Validators.required]),
   address:new FormControl('',[Validators.required])


  }) 
}
