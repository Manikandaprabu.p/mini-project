import { Injectable } from '@angular/core';
// import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Port } from '../model/port';
import { Register } from '../model/register';
import { Db2dao } from '../model/db2dao';

@Injectable({
  providedIn: 'root'
})
export class FormValidationService{
  
  private URLRegisteration="http://localhost:8080";
  constructor(public httpClient:HttpClient) { }

  
  createRegisteration(port:Port):Observable<Port>
  {
      return this.httpClient.post<Port>((this.URLRegisteration+"/hello/add"),port);
  }

  createlogin(register:Register):Observable<Register>
  {
      return this.httpClient.post<Register>('http://localhost:8080/hello/addData',register);
  }

  createmongo(db2dao:Db2dao):Observable<Db2dao>
  {
      return this.httpClient.post<Db2dao>('http://localhost:8080/hello/addMongoData',db2dao);
  }

  createKafka(db2dao:Db2dao):Observable<Db2dao>
  {
      return this.httpClient.post<Db2dao>('http://localhost:8080/hello/postData',db2dao);
  }

  getDetails()
  {
      return this.httpClient.get<Port[]>('http://localhost:8080/hello/get');
  }
  // get(port:number)
  // {
  //     return this.httpClient.get<string>('http://localhost:8080/hello/sts/'+port);
      
  // }
 
  
  get(element:Port): Observable<Port> {
    let port=element.port;
    let ip=element.ip;
    return this.httpClient.get<Port>('http://localhost:8080/hello/sts/'+port+"/"+ip);
  }


  
  getMongoDetails()
  {
      return this.httpClient.get<Db2dao[]>('http://localhost:8080/hello/getDataMongo');
  }
    
  
  getEmail(element:Register): Observable<Register> {
    let email=element.email;
    return this.httpClient.get<Register>('http://localhost:8070/api/hii/'+email);
  }

}
