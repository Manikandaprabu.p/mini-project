import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { MaterialDesignModule } from './moduleM/material-design/material-design.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './component/register/register.component';
import { WelcomeComponent } from './component/welcome/welcome.component';
import { StatusComponent } from './component/status/status.component';
import { NavicationComponent } from './component/navication/navication.component';
import { ChartComponent } from './component/chart/chart.component';
import { EmailComponent } from './component/email/email.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CheckStatusComponent } from './component/check-status/check-status.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    WelcomeComponent,
    StatusComponent,
    NavicationComponent,
    ChartComponent,
    EmailComponent,
    CheckStatusComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialDesignModule,
    BrowserAnimationsModule,
    ReactiveFormsModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
